<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * Plugin Name: CryptoWoo Dash Add-on
 * Plugin URI: https://www.cryptowoo.com/shop/cryptowoo-dash-addon
 * Description: Accept DASH payments in WooCommerce. Requires CryptoWoo main plugin and CryptoWoo HD Wallet Add-on.
 * Version: 0.3.4
 * Author: flxstn
 * Author URI: https://www.cryptowoo.com
 * License: GPLv2
 * Text Domain: cryptowoo-dash-addon
 * Domain Path: /lang
 * WC tested up to: 4.2.2
 */

define( 'CWDASH_VER', '0.3.4' );

// Load the plugin update library.
add_action( 'cryptowoo_api_manager_loaded', function () {
	new CW_License_Menu( __FILE__, 2413, CWDASH_VER );
} );

/**
 * Plugin activation
 */
function cryptowoo_dash_addon_activate() {

	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

	$hd_add_on_file = 'cryptowoo-hd-wallet-addon/cryptowoo-hd-wallet-addon.php';
	if ( ! file_exists( WP_PLUGIN_DIR . '/' . $hd_add_on_file ) || ! file_exists( WP_PLUGIN_DIR . '/cryptowoo/cryptowoo.php' ) ) {

		// If WooCommerce is not installed then show installation notice
		add_action( 'admin_notices', 'cryptowoo_dash_notinstalled_notice' );

		return;
	} elseif ( ! is_plugin_active( $hd_add_on_file ) ) {
		add_action( 'admin_notices', 'cryptowoo_dash_inactive_notice' );

		return;
	}

	if( (defined('CWOO_VERSION' ) && version_compare(CWOO_VERSION, '0.22.0', '<')) || (defined('HDWALLET_VER' ) && version_compare(HDWALLET_VER, '0.9.1', '<')) ) {
		deactivate_plugins( '/cryptowoo-dash-addon/cryptowoo-dash-addon.php', true );
	}
}

register_activation_hook( __FILE__, 'cryptowoo_dash_addon_activate' );
add_action( 'admin_init', 'cryptowoo_dash_addon_activate' );

/**
 * CryptoWoo inactive notice
 */
function cryptowoo_dash_inactive_notice() {

	?>
    <div class="error">
        <p><?php _e( '<b>CryptoWoo Dash add-on error!</b><br>It seems like the CryptoWoo HD Wallet add-on has been deactivated.<br>
       				Please go to the Plugins menu and make sure that the CryptoWoo HD Wallet add-on is activated.', 'cryptowoo-dash-addon' ); ?></p>
    </div>
	<?php
}


/**
 * CryptoWoo HD Wallet add-on not installed notice
 */
function cryptowoo_dash_notinstalled_notice() {
	$addon_link = '<a href="https://www.cryptowoo.com/shop/cryptowoo-hd-wallet-addon/" target="_blank">CryptoWoo HD Wallet add-on</a>';
	?>
    <div class="error">
        <p><?php printf( __( '<b>CryptoWoo Dash add-on error!</b><br>It seems like the CryptoWoo HD Wallet add-on is not installed.<br>
					The CryptoWoo Dash add-on will only work in combination with the CryptoWoo main plugin and the %s.', 'cryptowoo-dash-addon' ), $addon_link ); ?></p>
    </div>
	<?php
}

function cwdash_hd_enabled() {
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

	return is_plugin_active( 'cryptowoo-hd-wallet-addon/cryptowoo-hd-wallet-addon.php' ) && is_plugin_active( 'cryptowoo/cryptowoo.php' );
}

if ( cwdash_hd_enabled() ) {
	// Coin symbol and name
	add_filter( 'woocommerce_currencies', 'cwdash_woocommerce_currencies', 10, 1 );
	add_filter( 'cw_get_currency_symbol', 'cwdash_get_currency_symbol', 10, 2 );
	add_filter( 'cw_get_enabled_currencies', 'cwdash_add_coin_identifier', 10, 1 );

	// BIP32 prefixes
	add_filter( 'address_prefixes', 'cwdash_address_prefixes', 10, 1 );

	// Custom block explorer URL
	add_filter( 'cw_link_to_address', 'cwdash_link_to_address', 10, 4 );

	// Options page validations
	add_filter( 'validate_custom_api_genesis', 'cwdash_validate_custom_api_genesis', 10, 2 );
	add_filter( 'validate_custom_api_currency', 'cwdash_validate_custom_api_currency', 10, 2 );
	add_filter( 'cryptowoo_is_ready', 'cwdash_cryptowoo_is_ready', 10, 3 );
	add_filter( 'cw_misconfig_notice', 'cwdash_cryptowoo_misconfig_notice', 10, 2 );

	// HD wallet management
	add_filter( 'index_key_ids', 'cwdash_index_key_ids', 10, 1 );
	add_filter( 'mpk_key_ids', 'cwdash_mpk_key_ids', 10, 1 );
	add_filter( 'get_mpk_data_mpk_key', 'cwdash_get_mpk_data_mpk_key', 10, 3 );
	add_filter( 'get_mpk_data_network', 'cwdash_get_mpk_data_network', 10, 3 );
	add_filter( 'cw_blockcypher_currencies', 'cwdash_add_currency_to_array', 10, 1 );
	add_filter( 'cw_discovery_notice', 'cwdash_add_currency_to_array', 10, 1 );

	// Currency params
	add_filter( 'cw_get_currency_params', 'cwdash_get_currency_params', 10, 2 );

	// Order sorting and prioritizing
	add_filter( 'cw_sort_unpaid_addresses', 'cwdash_sort_unpaid_addresses', 10, 2);
	add_filter( 'cw_prioritize_unpaid_addresses', 'cwdash_prioritize_unpaid_addresses', 10, 2);
	add_filter( 'cw_filter_batch', 'cwdash_filter_batch', 10, 2);

	// Add discovery button to currency option
	add_filter( 'redux/options/cryptowoo_payments/field/cryptowoo_dash_mpk', 'hd_wallet_discovery_button' );

	// Exchange rates
	add_filter( 'cw_force_update_exchange_rates', 'cwdash_force_update_exchange_rates', 10, 2 );
	add_filter( 'cw_cron_update_exchange_data', 'cwdash_cron_update_exchange_data', 10, 2 );



	// Catch failing processing API (only if processing_fallback is enabled)
	add_filter( 'cw_get_tx_api_config', 'cwdash_cw_get_tx_api_config', 10, 3 );

	// Insight API URL
	add_filter( 'cw_prepare_insight_api', 'cwdash_override_insight_url', 10, 4 );

	// Wallet config
	add_filter( 'wallet_config', 'cwdash_wallet_config', 10, 3 );
	add_filter( 'cw_get_processing_config', 'cwdash_processing_config', 10, 3 );

	// Options page
	add_action( 'plugins_loaded', 'cwdash_add_fields', 10 );


}

/**
 * Dash font color for aw-cryptocoins
 * see cryptowoo/assets/fonts/aw-cryptocoins/cryptocoins-colors.css
 */
function cwdash_coin_icon_color( ) { ?>
	<style type="text/css">.cc-DASH, .cc-DASH-alt { color: #1c75bc; }</style>
<?php }
add_action('wp_head', 'cwdash_coin_icon_color');

/**
 * Processing API configuration error
 *
 * @param $enabled
 * @param $options
 *
 * @return mixed
 */
function cwdash_cryptowoo_misconfig_notice( $enabled, $options ) {
	$enabled['DASH'] = $options['processing_api_dash'] === 'disabled' && ( (bool) CW_Validate::check_if_unset( 'cryptowoo_dash_mpk_xpub', $options ) || (bool) CW_Validate::check_if_unset( 'cryptowoo_dash_mpk', $options ) );

	return $enabled;
}

/**
 * Add currency name
 *
 * @param $currencies
 *
 * @return mixed
 */
function cwdash_woocommerce_currencies( $currencies ) {
	$currencies['DASH'] = __( 'Dash', 'cryptowoo' );

	return $currencies;
}


/**
 * Add currency symbol
 *
 * @param $currency_symbol
 * @param $currency
 *
 * @return string
 */
function cwdash_get_currency_symbol( $currency_symbol, $currency ) {
	return $currency === 'DASH' ? 'DASH' : $currency_symbol;
}


/**
 * Add coin identifier
 *
 * @param $coin_identifiers
 *
 * @return array
 */
function cwdash_add_coin_identifier( $coin_identifiers ) {
	$coin_identifiers['DASH'] = 'dash';

	return $coin_identifiers;
}


/**
 * Add address prefix
 *
 * @param $prefixes
 *
 * @return array
 */
function cwdash_address_prefixes( $prefixes ) {
	$prefixes['DASH'] = '4c';

	return $prefixes;
}


/**
 * Add wallet config
 *
 * @param $wallet_config
 * @param $currency
 * @param $options
 *
 * @return array
 */
function cwdash_wallet_config( $wallet_config, $currency, $options ) {
	if ( $currency === 'DASH' ) {
		$wallet_config                       = array(
			'coin_client'   => 'dash',
			'request_coin'  => 'DASH',
			'multiplier'    => CW_Validate::check_if_unset('multiplier_dash', $options, 1.00 ),
			'safe_address'  => false,
			'decimals'      => 8,
			'amount_base_units' => 1e8,
			'mpk_key'       => ! CW_Validate::check_if_unset( 'cryptowoo_dash_mpk_xpub', $options, false ) ? 'cryptowoo_dash_mpk' : 'cryptowoo_dash_mpk_xpub',
			'fwd_addr_key'  => 'safe_dash_address',
			'threshold_key' => 'forwarding_threshold_dash'
		);
		$wallet_config['hdwallet']           = CW_Validate::check_if_unset( $wallet_config['mpk_key'], $options, false );
		$wallet_config['coin_protocols'][]   = 'dash';
		$wallet_config['forwarding_enabled'] = false;
	}

	return $wallet_config;
}

/**
 * Add InstantSend and "raw" zeroconf settings to processing config
 *
 * @param $pc_conf
 * @param $currency
 * @param $options
 *
 * @return array
 */
function cwdash_processing_config( $pc_conf, $currency, $options ) {
	if ( $currency === 'DASH' ) {
		$pc_conf['instant_send']       = isset( $options['dash_instant_send'] ) ? (bool) $options['dash_instant_send'] : false;
		$pc_conf['instant_send_depth'] = 5; // TODO Maybe add option

		// Maybe accept "raw" zeroconf
		$pc_conf['min_confidence'] = isset( $options['cryptowoo_dash_min_conf'] ) && (int) $options['cryptowoo_dash_min_conf'] === 0 && isset( $options['dash_raw_zeroconf'] ) && (bool) $options['dash_raw_zeroconf'] ? 0 : 1;
	}

	return $pc_conf;
}


/**
 * Override links to payment addresses
 *
 * @param $url
 * @param $address
 * @param $currency
 * @param $options
 *
 * @return string
 */
function cwdash_link_to_address( $url, $address, $currency, $options ) {
	if ( $currency === 'DASH' ) {
		$url = "https://explorer.dash.org/address/{$address}";
		/*if ( $options['preferred_block_explorer_dash'] === 'autoselect' ) {
			    $url = "https://explorer.dash.org/address/{$address}";
		}*/
		if ( $options['preferred_block_explorer_dash'] === 'custom' && isset( $options['custom_block_explorer_dash'] ) ) {
			$url = preg_replace( '/{{ADDRESS}}/', $address, $options['custom_block_explorer_dash'] );
			if ( ! wp_http_validate_url( $url ) ) {
				$url = '#';
			}
		} elseif ( $options[ 'preferred_block_explorer_bch' ] === 'blockchair' ) {
			$url     = "https://blockchair.com/dash/address/{$address}";
		}
	}

	return $url;
}


/**
 * Override genesis block
 *
 * @param $genesis
 * @param $field_id
 *
 * @return string
 */
function cwdash_validate_custom_api_genesis( $genesis, $field_id ) {
	if ( in_array( $field_id, array( 'custom_api_dash', 'processing_fallback_url_dash' ) ) ) {
		$genesis = '00000ffd590b1485b3caadc19b22e6379c733355108f107a430458cdf3407ab6';
	}

	return $genesis;
}


/**
 * Override custom API currency
 *
 * @param $currency
 * @param $field_id
 *
 * @return string
 */
function cwdash_validate_custom_api_currency( $currency, $field_id ) {
	if ( in_array( $field_id, array( 'custom_api_dash', 'processing_fallback_url_dash' ) ) ) {
		$currency = 'DASH';
	}

	return $currency;
}


/**
 * Add currency to cryptowoo_is_ready
 *
 * @param $enabled
 * @param $options
 * @param $changed_values
 *
 * @return array
 */
function cwdash_cryptowoo_is_ready( $enabled, $options, $changed_values ) {
	$enabled['DASH']           = (bool) CW_Validate::check_if_unset( 'cryptowoo_dash_mpk', $options, false ) ?: (bool) CW_Validate::check_if_unset( 'cryptowoo_dash_mpk_xpub', $options, false );
	$enabled['DASH_transient'] = (bool) CW_Validate::check_if_unset( 'cryptowoo_dash_mpk', $changed_values, false ) ?: (bool) CW_Validate::check_if_unset( 'cryptowoo_dash_mpk_xpub', $changed_values, false );

	return $enabled;
}


/**
 * Add currency to is_cryptostore check
 *
 * @param $cryptostore
 * @param $woocommerce_currency
 *
 * @return bool
 */
function cwdash_is_cryptostore( $cryptostore, $woocommerce_currency ) {
	return (bool) $cryptostore ?: $woocommerce_currency === 'DASH';
}

add_filter( 'is_cryptostore', 'cwdash_is_cryptostore', 10, 2 );

/**
 * Add HD index key id for currency
 *
 * @param $index_key_ids
 *
 * @return array
 */
function cwdash_index_key_ids( $index_key_ids ) {
	$index_key_ids['DASH'] = 'cryptowoo_dash_index';

	return $index_key_ids;
}


/**
 * Add HD mpk key id for currency
 *
 * @param $mpk_key_ids
 *
 * @return array
 */
function cwdash_mpk_key_ids( $mpk_key_ids ) {
	$mpk_key_ids['DASH'] = 'cryptowoo_dash_mpk';
	$mpk_key_ids['DASH_E'] = 'cryptowoo_dash_mpk_xpub';

	return $mpk_key_ids;
}


/**
 * Override mpk_key
 *
 * @param $mpk_key
 * @param $currency
 * @param $options
 *
 * @return string
 */
function cwdash_get_mpk_data_mpk_key( $mpk_key, $currency, $options ) {
	if ( $currency === 'DASH' ) {
		if ( isset( $options['cryptowoo_dash_mpk_xpub'] ) && $options['cryptowoo_dash_mpk_xpub'] !== '' ) {
			$mpk_key = "cryptowoo_dash_mpk_xpub";
		} else {
			$mpk_key = "cryptowoo_dash_mpk";
		}
	}

	return $mpk_key;
}


/**
 * Override mpk_data->network and add extended public key prefixes
 *
 * @param $mpk_data
 * @param $currency
 * @param $options
 *
 * @return object
 */
function cwdash_get_mpk_data_network( $mpk_data, $currency, $options ) {

	require_once __DIR__."/DashRegistry.php";

	if ( $currency === 'DASH' ) {

		$mpk_data->network = BitWasp\Bitcoin\Network\NetworkFactory::dash(); //create( '4c', '10', 'cc' )->setHDPubByte( '02fe52f8' )->setHDPrivByte( '02fe52cc' )->setNetMagicBytes( 'bd6b0cbf' );

		// Add Dash extended public key prefixes
		$mpk_data->dashPrefixes = new \BitWasp\Bitcoin\Network\Slip132\DashRegistry(); // TODO For some reason this does not add the drkp/drkv prefixes

		// Swap prefix to xpub
		if($mpk_data->mpk && false === strpos($mpk_data->mpk, 'xpub')) {
			$mpk_data->mpk = cwdash_swap_prefix($mpk_data->mpk, 'xpub'); // TODO Remove this and use slip132 above
		}

		$mpk_data->network_config = new \BitWasp\Bitcoin\Key\Deterministic\HdPrefix\NetworkConfig($mpk_data->network, [
			$mpk_data->slip132->p2pkh($mpk_data->dashPrefixes)
		]);

	}

	return $mpk_data;
}

function cwdash_swap_prefix($xkey, $prefix) {

	$version_prefixes = [
		'drkv' => '02fe52f8',
		'drkp' => '02fe52cc',
		'DRKV' => '3a8061a0',
		'DRKP' => '3a805837',
		'xpub' => '0488b21e',
		'xprv' => '0488ade4',
	];

	if(!array_key_exists($prefix, $version_prefixes)) {
		return false;
	}

	try {
		$hex = BitWasp\Bitcoin\Base58::decodeCheck($xkey)->getHex();
	}
	catch (\Exception $e) {
		$hex = 'Invalid base58 data';
	}

	$nh = $version_prefixes[$prefix] . substr($hex, 8, 156);
	$new_xkey = BitWasp\Bitcoin\Base58::encodeCheck(BitWasp\Buffertools\Buffer::hex($nh));

	return $new_xkey;
}

/**
 * Add currency force exchange rate update button
 *
 * @param $results
 *
 * @return array
 */
function cwdash_force_update_exchange_rates( $results ) {
	$results['dash'] = CW_ExchangeRates::processing()->update_coin_rates( 'DASH', false, true );

	return $results;
}

/**
 * Add currency to background exchange rate update
 *
 * @param $data
 * @param $options
 *
 * @return array
 */
function cwdash_cron_update_exchange_data( $data, $options ) {
	$dash = CW_ExchangeRates::processing()->update_coin_rates( 'DASH', $options );

	if ( array_key_exists( 'status', $dash ) && ( $dash['status'] === 'not updated' || strpos( $dash['status'], 'disabled' ) ) ) {
		$data['dash'] = strpos( $dash['status'], 'disabled' ) ? $dash['status'] : $dash['last_update'];
	} else {
		$data['dash'] = $dash;
	}

	return $data;
}

/**
 * Add currency to blockcypher currencies
 *
 * @param $currencies
 *
 * @return array
 */
function cwdash_add_currency_to_array( $currencies ) {
	$currencies[] = 'DASH';

	return $currencies;
}


/**
 * Override currency params in xpub validation
 *
 * @param $currency_params
 * @param $field_id
 *
 * @return object
 */
function cwdash_get_currency_params( $currency_params, $field_id ) {
	if ( strcmp( $field_id, 'cryptowoo_dash_mpk_xpub' ) === 0 ) {
		$currency_params                     = new stdClass();
		$currency_params->mand_mpk_prefix    = 'xpub';    // mpk xpub prefix
		$currency_params->mand_base58_prefix = '0488b21e'; // base58 xpub prefix
		$currency_params->currency           = 'DASH';
		$currency_params->index_key          = 'cryptowoo_dash_index';
		$currency_params->strlen             = 111;
	} elseif ( strcmp( $field_id, 'cryptowoo_dash_mpk' ) === 0 ) {
		$currency_params                     = new stdClass();
		$currency_params->mand_mpk_prefix    = 'drkv';    // mpk xpub prefix
		$currency_params->mand_base58_prefix = '02fe52f8'; // base58 xpub prefix
		$currency_params->currency           = 'DASH';
		$currency_params->index_key          = 'cryptowoo_dash_index';
		$currency_params->strlen             = 111;
	}

	return $currency_params;
}

/**
 * Add DASH addresses to sort unpaid addresses
 *
 * @param array $top_n
 * @param mixed $address
 *
 * @return array
 */
function cwdash_sort_unpaid_addresses($top_n, $address) {
	if (strcmp($address->payment_currency, 'DASH') === 0) {
		$top_n[1]['DASH'][]      = $address;
	}
	return $top_n;
}

/**
 * Add DASH addresses to prioritize unpaid addresses
 *
 * @param array $top_n
 * @param mixed $address
 *
 * @return array
 */
function cwdash_prioritize_unpaid_addresses($top_n, $address) {
	if (strcmp($address->payment_currency, 'DASH') === 0) {
		$top_n[1][]      = $address;
	}
	return $top_n;
}

/**
 * Add DASH addresses to address_batch
 *
 * @param array $address_batch
 * @param mixed $address
 *
 * @return array
 */
function cwdash_filter_batch($address_batch, $address) {
	if (strcmp($address->payment_currency, 'DASH') === 0) {
		$address_batch['DASH'][] = $address->address;
	}
	return $address_batch;
}

/**
 * Fallback on failing API
 *
 * @param $api_config
 * @param $currency
 *
 * @return array
 */
function cwdash_cw_get_tx_api_config( $api_config, $currency ) {

	if ( $currency === 'DASH' ) {
		if ( $api_config->tx_update_api === 'blockcypher' ) {
			$api_config->tx_update_api   = 'insight';
			$api_config->skip_this_round = false;
		} else {
			$api_config->tx_update_api   = 'sochain';
			$api_config->skip_this_round = false;
		}
	}

	return $api_config;
}

/**
 * Override Insight API URL if no URL is found in the settings
 *
 * @param $insight
 * @param $endpoint
 * @param $currency
 * @param $options
 *
 * @return mixed
 */
function cwdash_override_insight_url( $insight, $endpoint, $currency, $options ) {
	if ( $currency === 'DASH' && isset( $options['processing_fallback_url_dash'] ) && wp_http_validate_url( $options['processing_fallback_url_dash'] ) ) {
		$fallback_url = $options['processing_fallback_url_dash'];
		$urls         = $endpoint ? CW_Formatting::format_insight_api_url( $fallback_url, $endpoint ) : CW_Formatting::format_insight_api_url( $fallback_url, '' );
		$insight->url = $urls['surl'];
	}

	return $insight;
}

/**
 * Add Redux options
 */
function cwdash_add_fields() {
	$woocommerce_currency = get_option( 'woocommerce_currency' );

	/*
	 * Required confirmations
	 */
	Redux::setField( 'cryptowoo_payments', array(
		'section_id' => 'processing-confirmations',
		'id'         => 'cryptowoo_dash_min_conf',
		'type'       => 'spinner',
		'title'      => sprintf( __( '%s Minimum Confirmations', 'cryptowoo' ), 'DASH' ),
		'desc'       => sprintf( __( 'Minimum number of confirmations for <strong>%s</strong> transactions - %s Confirmation Threshold', 'cryptowoo' ), 'Dash', 'Dash' ),
		'default'    => 5,
		'min'        => 0,
		'step'       => 1,
		'max'        => 100,
	) );

	Redux::setField( 'cryptowoo_payments', array(
		'section_id' => 'processing-confirmations',
		'id'         => 'dash_instant_send',
		'type'       => 'switch',
		'title'      => __( 'DASH InstantSend', 'cryptowoo' ),
		'subtitle'   => sprintf( __( 'Dash offers an "instant" transaction method known as InstantX or InstantSend which may be significantly faster than normal Dash transactions. %sLearn more%s', 'cryptowoo' ), '<a href="https://www.cryptowoo.com/dash-instantsend/" target="_blank">', '</a>' ),
		'desc'       => __( 'If this option is enabled, CryptoWoo will treat all InstantSend transactions as if they already received 5 confirmations in the blockchain. Disable this option to treat InstantSend transactions like any other transaction on the Dash network.', 'cryptowoo-dash-addon' ),
		'default'    => true,
		'required'   => array( 'processing_api_dash', '=', 'custom' )
	) );

	Redux::setField( 'cryptowoo_payments', array(
		'section_id' => 'processing-confirmations',
		'id'         => 'dash_raw_zeroconf',
		'type'       => 'switch',
		'title'      => __( 'DASH "Raw" Zeroconf', 'cryptowoo' ),
		'subtitle'   => __( 'Accept unconfirmed Dash transactions as soon as they are seen on the network.', 'cryptowoo' ),
		'desc'       => sprintf( __( '%sThis practice is generally not recommended. Only enable this if you know what you are doing!%s', 'cryptowoo' ), '<strong>', '</strong>' ),
		'default'    => false,
		'required'   => array(
			//array('processing_api_dash', '=', 'custom'),
			array( 'cryptowoo_dash_min_conf', '=', 0 )
		),
	) );


	/*
	 * Zeroconf order amount threshold
	 */
	Redux::setField( 'cryptowoo_payments', array(
		'section_id' => 'processing-zeroconf',
		'id'         => 'cryptowoo_max_unconfirmed_dash',
		'type'       => 'slider',
		'title'      => sprintf( __( '%s zeroconf threshold (%s)', 'cryptowoo' ), 'DASH', $woocommerce_currency ),
		'desc'       => '',
		'required'   => array( 'cryptowoo_dash_min_conf', '<', 1 ),
		'default'    => 100,
		'min'        => 0,
		'step'       => 10,
		'max'        => 500,
	) );

	Redux::setField( 'cryptowoo_payments', array(
		'section_id' => 'processing-zeroconf',
		'id'         => 'cryptowoo_dash_zconf_notice',
		'type'       => 'info',
		'style'      => 'info',
		'notice'     => false,
		'required'   => array( 'cryptowoo_dash_min_conf', '>', 0 ),
		'icon'       => 'fa fa-info-circle',
		'title'      => sprintf( __( '%s Zeroconf Threshold Disabled', 'cryptowoo' ), 'Dash' ),
		'desc'       => sprintf( __( 'This option is disabled because you do not accept unconfirmed %s payments.', 'cryptowoo' ), 'Dash' ),
	) );


	/*
	// Remove 3rd party confidence
	Redux::removeField( 'cryptowoo_payments', 'custom_api_confidence', false );

	/*
	 * Confidence warning
	 * /
	Redux::setField( 'cryptowoo_payments', array(
		'section_id'        => 'processing-confidence',
			'id'    => 'dash_confidence_warning',
			'type'  => 'info',
			'title' => __('Be careful!', 'cryptowoo'),
			'style' => 'warning',
			'desc'  => __('Accepting transactions with a low confidence value increases your exposure to double-spend attacks. Only proceed if you don\'t automatically deliver your products and know what you\'re doing.', 'cryptowoo'),
			'required' => array('min_confidence_dash', '<', 95)
	));

	/*
	 * Transaction confidence
	 * /

	Redux::setField( 'cryptowoo_payments', array(
			'section_id'        => 'processing-confidence',
			'id'      => 'min_confidence_dash',
			'type'    => 'switch',
			'title'   => sprintf(__('%s transaction confidence (%s)', 'cryptowoo'), 'DASH', '%'),
			//'desc'    => '',
			'required' => array('cryptowoo_dash_min_conf', '<', 1),

	));


	Redux::setField( 'cryptowoo_payments', array(
		'section_id' => 'processing-confidence',
		'id'      => 'min_confidence_dash_notice',
		'type'    => 'info',
		'style' => 'info',
		'notice'    => false,
		'required' => array('cryptowoo_dash_min_conf', '>', 0),
		'icon'  => 'fa fa-info-circle',
		'title'   => sprintf(__('%s "Raw" Zeroconf Disabled', 'cryptowoo'), 'Dash'),
		'desc'    => sprintf(__('This option is disabled because you do not accept unconfirmed %s payments.', 'cryptowoo'), 'Dash'),
	));

	// Re-add 3rd party confidence
	Redux::setField( 'cryptowoo_payments', array(
		'section_id'        => 'processing-confidence',
		'id'       => 'custom_api_confidence',
		'type'     => 'switch',
		'title'    => __('Third Party Confidence Metrics', 'cryptowoo'),
		'subtitle' => __('Enable this to use the SoChain confidence metrics when accepting zeroconf transactions with your custom Bitcoin, Litecoin, or Dogecoin API.', 'cryptowoo'),
		'default'  => false,
	));
    */

	// Remove blockcypher token field
	Redux::removeField( 'cryptowoo_payments', 'blockcypher_token', false );
	// Remove CryptoID token field
	Redux::removeField( 'cryptowoo_payments', 'cryptoid_api_key', false );

	/*
	 * Processing API
	 */
	Redux::setField( 'cryptowoo_payments', array(
		'section_id'        => 'processing-api',
		'id'                => 'processing_api_dash',
		'type'              => 'select',
		'title'             => sprintf( __( '%s Processing API', 'cryptowoo' ), 'DASH' ),
		'subtitle'          => sprintf( __( 'Choose the API provider you want to use to look up %s payments.', 'cryptowoo' ), 'Dash' ),
		'options'           => array(
			'dashorg'     => 'Dash.org (explorer.dash.org)',
			'sochain'     => 'SoChain (One address per interval per currency)',
			'blockcypher' => 'BlockCypher.com',
			'custom'      => __( 'Custom (no testnet)', 'cryptowoo' ),
			'disabled'    => __( 'Disabled', 'cryptowoo' ),
		),
		'desc'              => '',
		'default'           => 'disabled',
		'ajax_save'         => false, // Force page load when this changes
		'validate_callback' => 'redux_validate_processing_api',
		'select2'           => array( 'allowClear' => false ),
	) );

	/*
	 * Processing API custom URL warning
	 */
	Redux::setField( 'cryptowoo_payments', array(
		'section_id' => 'processing-api',
		'id'         => 'processing_api_dash_info',
		'type'       => 'info',
		'style'      => 'critical',
		'icon'       => 'el el-warning-sign',
		'required'   => array(
			array( 'processing_api_dash', 'equals', 'custom' ),
			array( 'custom_api_dash', 'equals', '' ),
		),
		'desc'       => sprintf( __( 'Please enter a valid URL in the field below to use a custom %s processing API', 'cryptowoo' ), 'Dash' ),
	) );

	/*
	 * Custom processing API URL
	 */
	Redux::setField( 'cryptowoo_payments', array(
		'section_id'        => 'processing-api',
		'id'                => 'custom_api_dash',
		'type'              => 'text',
		'title'             => sprintf( __( '%s Insight API URL', 'cryptowoo' ), 'DASH' ),
		'subtitle'          => sprintf( __( 'Connect to any %sInsight API%s instance.', 'cryptowoo' ), '<a href="https://github.com/bitpay/insight-api/" title="Insight API" target="_blank">', '</a>' ),
		'desc'              => sprintf( __( 'The root URL of the API instance:%sLink to address:%shttps://insight.dash.org/insight-api/addr/XtuVUju4Baaj7YXShQu4QbLLR7X2aw9Gc8%sRoot URL: %shttps://insight.dash.org/insight-api/%s', 'cryptowoo-dash-addon' ), '<p>', '<code>', '</code><br>', '<code>', '</code></p>' ),
		'placeholder'       => 'https://insight.dash.org/insight-api/',
		'required'          => array( 'processing_api_dash', 'equals', 'custom' ),
		'validate_callback' => 'redux_validate_custom_api',
		'ajax_save'         => false,
		'msg'               => __( 'Invalid DASH Insight API URL', 'cryptowoo' ),
		'default'           => '',
		'text_hint'         => array(
			'title'   => 'Please Note:',
			'content' => __( 'Make sure the root URL of the API has a trailing slash ( / ).', 'cryptowoo' ),
		)
	) );

	// Re-add blockcypher token field
	Redux::setField( 'cryptowoo_payments', array(
		'section_id'        => 'processing-api',
		'id'                => 'blockcypher_token',
		'type'              => 'text',
		'ajax_save'         => false, // Force page load when this changes
		'desc'              => sprintf( __( '%sMore info%s', 'cryptowoo' ), '<a href="http://dev.blockcypher.com/#rate-limits-and-tokens" title="BlockCypher Docs: Rate limits and tokens" target="_blank">', '</a>' ),
		'title'             => __( 'BlockCypher Token (optional)', 'cryptowoo' ),
		'subtitle'          => sprintf( __( 'Use the API token from your %sBlockCypher%s account.', 'cryptowoo' ), '<strong><a href="https://accounts.blockcypher.com/" title="BlockCypher account dashboard" target="_blank">', '</a></strong>' ),
		'validate_callback' => 'redux_validate_token'
	) );
	// Re-add CryptoID token field
	Redux::setField( 'cryptowoo_payments', array(
		'section_id' => 'processing-api',
		'id'         => 'cryptoid_api_key',
		'type'       => 'text',
		'ajax_save'  => false, // Force page load when this changes
		'desc'       => sprintf(__('%sMore info%s', 'cryptowoo'), '<a href="https://chainz.cryptoid.info/api.dws" title="cryptoID API Docs" target="_blank">', '</a>'),
		'title'      =>  __('cryptoID API Key (required)', 'cryptowoo'),
		'subtitle'   => sprintf(__('Use the API token from your %sCryptoID%s account.', 'cryptowoo'), '<strong><a href="https://chainz.cryptoid.info/api.key.dws" title="Request cryptoID API Key" target="_blank">', '</a></strong>'),
		//'validate_callback' => 'redux_validate_token',
	) );

	// API Resource control information
	Redux::setField( 'cryptowoo_payments', array(
		'section_id'        => 'processing-api-resources',
		'id'                => 'processing_fallback_url_dash',
		'type'              => 'text',
		'title'             => sprintf( __( 'BlockCypher Dash API Fallback', 'cryptowoo' ), 'DASH' ),
		'subtitle'          => sprintf( __( 'Fallback to any %sInsight API%s instance in case the BlockCypher API fails. Retry BlockCypher upon beginning of the next hour. Leave empty to disable.', 'cryptowoo' ), '<a href="https://github.com/bitpay/insight-api/" title="Insight API" target="_blank">', '</a>' ),
		'desc'              => sprintf( __( 'The root URL of the API instance:%sLink to address:%shttps://insight.dash.org/insight-api/addr/XtuVUju4Baaj7YXShQu4QbLLR7X2aw9Gc8%sRoot URL: %shttps://insight.dash.org/insight-api/%s', 'cryptowoo-dash-addon' ), '<p>', '<code>', '</code><br>', '<code>', '</code></p>' ),
		'placeholder'       => 'https://insight.dash.org/insight-api/',
		'required'          => array( 'processing_api_dash', 'equals', 'blockcypher' ),
		'validate_callback' => 'redux_validate_custom_api',
		'ajax_save'         => false,
		'msg'               => __( 'Invalid DASH Insight API URL', 'cryptowoo' ),
		'default'           => 'https://insight.dash.org/insight-api/',
		'text_hint'         => array(
			'title'   => 'Please Note:',
			'content' => __( 'Make sure the root URL of the API has a trailing slash ( / ).', 'cryptowoo' ),
		)
	) );
	/*
	 * Preferred exchange rate provider
	 */
	Redux::setField( 'cryptowoo_payments', array(
		'section_id'        => 'rates-exchange',
		'id'                => 'preferred_exchange_dash',
		'type'              => 'select',
		'title'             => 'Dash Exchange (DASH/BTC)',
		'subtitle'          => sprintf( __( 'Choose the exchange you prefer to use to calculate the %sDash to Bitcoin exchange rate%s', 'cryptowoo' ), '<strong>', '</strong>.' ),
		'desc'              => sprintf( __( 'Cross-calculated via BTC/%s', 'cryptowoo' ), $woocommerce_currency ),
		'options'           => array(
		    'coingecko'  => 'CoinGecko',
			'shapeshift' => 'ShapeShift',
			'poloniex'   => 'Poloniex',
			'bittrex'    => 'Bittrex'
		),
		'default'           => 'coingecko',
		'ajax_save'         => false, // Force page load when this changes
		'validate_callback' => 'redux_validate_exchange_api',
		'select2'           => array( 'allowClear' => false )
	) );

	/*
	 * Exchange rate multiplier
	 */
	Redux::setField( 'cryptowoo_payments', array(
		'section_id'    => 'rates-multiplier',
		'id'            => 'multiplier_dash',
		'type'          => 'slider',
		'title'         => sprintf( __( '%s exchange rate multiplier', 'cryptowoo' ), 'Dash' ),
		'subtitle'      => sprintf( __( 'Extra multiplier to apply when calculating %s prices.', 'cryptowoo' ), 'Dash' ),
		'desc'          => '',
		'default'       => 1,
		'min'           => 1,
		'step'          => .001,
		'max'           => 2,
		'resolution'    => 0.001,
		'validate'      => 'comma_numeric',
		'display_value' => 'text'
	) );

	/*
	 * Preferred blockexplorer
	 */
	Redux::setField( 'cryptowoo_payments', array(
		'section_id' => 'rewriting',
		'id'         => 'preferred_block_explorer_dash',
		'type'       => 'select',
		'title'      => sprintf( __( '%s Block Explorer', 'cryptowoo' ), 'Dash' ),
		'subtitle'   => sprintf( __( 'Choose the block explorer you want to use for links to the %s blockchain.', 'cryptowoo' ), 'Dash' ),
		'desc'       => '',
		'options'    => array(
			'autoselect'        => __( 'Autoselect by processing API', 'cryptowoo' ),
			'explorer_dash_org' => 'explorer.dash.org',
			'blockchair'        => 'blockchair.com',
			'custom'            => __( 'Custom (enter URL below)' ),
		),
		'default'    => 'autoselect',
		'select2'    => array( 'allowClear' => false )
	) );

	Redux::setField( 'cryptowoo_payments', array(
		'section_id' => 'rewriting',
		'id'         => 'preferred_block_explorer_dash_info',
		'type'       => 'info',
		'style'      => 'critical',
		'icon'       => 'el el-warning-sign',
		'required'   => array(
			array( 'preferred_block_explorer_dash', '=', 'custom' ),
			array( 'custom_block_explorer_dash', '=', '' ),
		),
		'desc'       => sprintf( __( 'Please enter a valid URL in the field below to use a custom %s block explorer', 'cryptowoo' ), 'dash' ),
	) );
	Redux::setField( 'cryptowoo_payments', array(
		'section_id'        => 'rewriting',
		'id'                => 'custom_block_explorer_dash',
		'type'              => 'text',
		'title'             => sprintf( __( 'Custom %s Block Explorer URL', 'cryptowoo' ), 'Dash' ),
		'subtitle'          => __( 'Link to a block explorer of your choice.', 'cryptowoo' ),
		'desc'              => sprintf( __( 'The URL to the page that displays the information for a single address.%sPlease add %s{{ADDRESS}}%s as placeholder for the cryptocurrency address in the URL.%s', 'cryptowoo' ), '<br><strong>', '<code>', '</code>', '</strong>' ),
		'placeholder'       => 'http://live.blockcypher.com/btc/address/{{ADDRESS}}/',
		'required'          => array( 'preferred_block_explorer_dash', '=', 'custom' ),
		'validate_callback' => 'redux_validate_custom_blockexplorer',
		'ajax_save'         => false,
		'msg'               => __( 'Invalid custom block explorer URL', 'cryptowoo' ),
		'default'           => '',
	) );

	/*
	 * Currency Switcher plugin decimals
	 */
	Redux::setField( 'cryptowoo_payments', array(
		'section_id' => 'pricing-decimals',
		'id'         => 'decimals_DASH',
		'type'       => 'select',
		'title'      => sprintf( __( '%s amount decimals', 'cryptowoo' ), 'Dash' ),
		'subtitle'   => '',
		'desc'       => __( 'This option overrides the decimals option of the WooCommerce Currency Switcher plugin.', 'cryptowoo' ),
		'options'    => array(
			2 => '2',
			4 => '4',
			6 => '6',
			8 => '8'
		),
		'default'    => 2,
		'select2'    => array( 'allowClear' => false )
	) );


	/*
	 * HD wallet section start
	 */
	Redux::setField( 'cryptowoo_payments', array(
		'section_id' => 'wallets-hdwallet',
		'id'         => 'wallets-hdwallet-dash',
		'type'       => 'section',
		'title'      => __( 'DASH', 'cryptowoo-hd-wallet-addon' ),
		//'required' => array('testmode_enabled','equals','0'),
		'icon'       => 'cc-DASH',
		//'subtitle' => __('Use the field with the correct prefix of your Litecoin MPK. The prefix depends on the wallet client you used to generate the key.', 'cryptowoo-hd-wallet-addon'),
		'indent'     => true,
	) );

	/*
	 * Extended public key
	 */
	Redux::setField( 'cryptowoo_payments', array(
		'section_id'        => 'wallets-hdwallet',
		'id'                => 'cryptowoo_dash_mpk',
		'type'              => 'text',
		'ajax_save'         => false,
		'username'          => false,
		'desc' => __('Dash HD Wallet Extended Public Key', 'cryptowoo-hd-wallet-addon'),
		'title' => __('Dash HD Wallet Extended Public Key', 'cryptowoo-hd-wallet-addon'),
		'validate_callback' => 'redux_validate_mpk',
		'placeholder' => 'xpub...|drkv...|drkp...',
		//'required' => array('cryptowoo_dash_mpk', 'equals', ''),
		'text_hint'         => array(
			'title'   => 'Please Note:',
			'content' => sprintf( __( 'If you enter a used key you will have to run the address discovery process after saving this setting.%sUse a dedicated HD wallet (or at least a dedicated xpub) for your store payments to prevent address reuse.', 'cryptowoo-hd-wallet-addon' ), '<br>' ),
		)
	) );

	Redux::setField( 'cryptowoo_payments', array(
		'section_id'        => 'wallets-hdwallet',
		'id'         => 'derivation_path_dash',
		'type'       => 'select',
		'subtitle'   => '',
		'title'      => sprintf( __( '%s Derivation Path', 'cryptowoo-hd-wallet-addon' ), 'DASH' ),
		'desc'       => __('Change the derivation path to match the derivation path of your wallet client.', 'cryptowoo-hd-wallet-addon'),
		'validate_callback' => 'redux_validate_derivation_path',
		'options'    => array(
			'0/' => __('m/0/i (e.g. Electrum-DASH/TREZOR/Ledger Wallet)', 'cryptowoo-hd-wallet-addon'),
			'm' => __('m/i', 'cryptowoo-hd-wallet-addon'),
		),
		'default'    => '0/',
		'select2'    => array( 'allowClear' => false )
    ));


}

