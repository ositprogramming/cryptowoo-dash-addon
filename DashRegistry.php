<?php

namespace BitWasp\Bitcoin\Network\Slip132;

use BitWasp\Bitcoin\Key\Deterministic\Slip132\PrefixRegistry;
use BitWasp\Bitcoin\Script\ScriptType;

class DashRegistry extends PrefixRegistry
{
    public function __construct()
    {
        $map = [];

        foreach ([
                     // private, public
                     [["0488ade4", "0488b21e"], /* xpub */ [ScriptType::P2PKH]],
                     [["0488ade4", "0488b21e"], /* xpub */ [ScriptType::P2SH, ScriptType::P2PKH]],
                     [["02fe52cc", "02fe52f8"], /* drkv */ [ScriptType::P2SH, ScriptType::P2PKH]],
                     [["02fe52cc", "02fe52cc"], /* drkp */ [ScriptType::P2SH, ScriptType::P2PKH]],
                 ] as $row) {
            list ($prefixList, $scriptType) = $row;
            $type = implode("|", $scriptType);
            $map[$type] = $prefixList;
        }

        parent::__construct($map);
    }
}
